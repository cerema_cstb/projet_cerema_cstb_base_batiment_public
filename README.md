
ESPACE (nom temporaire) se veut une base de données agrégée et consolidée qui regroupe l'ensemble des informations relatives aux bâtiments publics et aux équipements tertiaires d'intérêt public ou collectif.

Elle centralise et synthétise des données provenant de N sources, offrant une vue d'ensemble, plus cohérente et accessible des actifs publics et para-publics. Elle vise à faciliter l'analyse de ce parc en réduisant les redondances et en harmonisant la structure.

Elle est en open-data.

Elle est rattachée au Référentiel National des Bâtiments et permet les liens avec les *bases* sources d'origine.

Elle vise à compléter les manques constatés dans les référentiels nationaux (bâti public non référencé dans les sources fiscales; absence de caractérisation dans la BDTopo).

Liste des sources intégrées:
- FINESS
- BASILIC
- Annuaire de l'éducation nationale
- Annuaire de l'administration
- établissements enseignement supérieur et recherche


Liste des sources prochainement intégrées:
- SIRENE
- BPE
- base des équipements sportifs