drop table if exists coriandre.basilic;
create table coriandre.basilic
("Source"	varchar,
"Nom"	varchar,
"Adresse"	varchar,
"Complement Adresse"	varchar,
"Code Postal"	varchar,
"libelle_geographique"	varchar,
"code_insee"	varchar,
"Code Insee Arrondt"	varchar,
"Identifiant origine"	varchar,
"Type équipement ou lieu"	varchar,
"Label et appellation"	varchar,
"Région"	varchar,
"Domaine"	varchar,
"Sous-domaine"	varchar,
"Archéologie détail"	varchar,
"Adresse postale"	varchar,
"Département"	varchar,
"Précision équipement"	varchar,
"N_Département"	varchar,
"Fonction_1"	varchar,
"Fonction_2"	varchar,
"Fonction_3"	varchar,
"Fonction_4"	varchar,
"Multiplexe"	varchar,
"Type_de_cinema"	varchar,
"Nombre_fauteuils_de_cinema"	varchar,
"Nombre_ecrans"	varchar,
"Nombre_de_salles_de_theatre"	varchar,
"Organisme_Siège_du_theatre"	varchar,
"Jauge_du_theatre"	varchar,
"Code_du_reseau_de_Bibliothèques"	varchar,
"Nom_du_Réseau_de_Bibliothèques"	varchar,
"Nom_de_l'Illustre"	varchar,
"Surface_Bibliothèque"	varchar,
"Rang"	varchar,
"Ident"	varchar,
"Identifiant_deps_à_partir_de_2022"	varchar,
"Libelle_EPCI"	varchar,
"Code_Insee_EPCI"	varchar,
"Démographie_sortie"	varchar,
"Démographie_détail"	varchar,
"Rang_fev24"	varchar,
"Identifiant_deps_old"	varchar,
"Précision protection sites et monuments"	varchar,
"GCD"	varchar,
"AAV"	varchar,
"Numéro de région"	varchar,
"Annee_Label_Appellation"	varchar,
"Latitude"	float,
"Longitude"	float,
"Demographie_entree_sortie"	varchar,
"Multi_equipements"	varchar,
"identifi_QPV"	varchar,
"coordonnees_geo"	varchar)

ALTER TABLE coriandre.basilic
add column point_geometry geometry(Point,4326);

UPDATE coriandre.basilic 
SET point_geometry = ST_SetSRID(ST_MakePoint("basilic"."Longitude", "basilic"."Latitude"), 4326);


















