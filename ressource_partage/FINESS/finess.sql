DROP TABLE IF EXISTS categorie_mapping;
CREATE TEMP TABLE categorie_mapping (
  groupe_categorie VARCHAR,
  categorie_2 VARCHAR,
  categorie_3 VARCHAR
);

INSERT INTO categorie_mapping(groupe_categorie, categorie_2, categorie_3) VALUES 
('Centres Hospitaliers',                                        'Santé et social',                             'Etablissement et service de santé'),
('Hôpitaux Locaux',                                             'Santé et social',                             'Etablissement et service de santé'),
('Centres Hospitaliers Spécialisés Lutte Maladies Mentales',    'Santé et social',                             'Etablissement et service de santé'),
('Etablissements de santé privé autorisés en SSR',              'Santé et social',                             'Autre service médico-social'),
('Etablissements de Soins de Courte Durée',                     'Santé et social',                             'Etablissement et service de santé'),
('Transfusion Sanguine',                                        'Santé et social',                             'Etablissement et service de santé'),
('Autres Etablissements Relevant de la Loi Hospitalière',       'Santé et social',                             'Etablissement et service de santé'),
('Etablissements de Soins de Longue Durée',                     'Santé et social',                             'Etablissement et service de santé'),
('Centres de Santé',                                            'Santé et social',                             'Etablissement et service de santé'),
('Dispensaires ou Centres de Soins',                            'Santé et social',                             'Etablissement et service de santé'),
('Centres de Lutte contre le Cancer',                           'Santé et social',                             'Etablissement et service de santé'),
('Centres Hospitaliers Régionaux',                              'Santé et social',                             'Etablissement et service de santé'),
('Etab.de soins relevant du service de santé des armées',       'Santé et social',                             'Etablissement et service de santé'),
('Maison de Naissance',                                         'Santé et social',                             'Etablissement et service de santé'),
('Etablissements ne relevant pas de la Loi Hospitalière',       'Santé et social',                             'Etablissement et service de santé'),
('Autres Etablissements de Lutte contre les Maladies Mentales', 'Santé et social',                             'Etablissement et service de santé'),
('Etablissements et services multi-clientèles',                 'Santé et social',                             'Etablissement et service de santé'),
('Laboratoires de Biologie Médicale',                           'Santé et social',                             'Autre établissement et service à caractère sanitaire'),
('Traitements et Soins à Domicile',                             'Santé et social',                             'Etablissement et service de santé'),
('Dialyse Ambulatoire',                                         'Santé et social',                             'Etablissement et service de santé'),
('Installations autonomes de chirurgie esthétique',             'Santé et social',                             'Etablissement et service de santé'),
('Etablissements d''Hébergement pour Personnes Âgées',          'Santé et social',                             'Etablissement médico-social pour personnes âgées'),
('Etab.et Services de Travail Protégé pour Adultes Handicapés', 'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Services Concourant à la Protection de l''Enfance',           'Santé et social',                             'Autre service médico-social'),
('Etab. et Services d''Hébergement pour Adultes Handicapés',    'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Services à Domicile ou Ambulatoires pour Handicapés',         'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etab.Expérimentaux en Faveur des Adultes Handicapés',         'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Services de Maintien à Domicile pour Handicapés',             'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etablissements pour Adultes et Familles en Difficulté',       'Santé et social',                             'Autre service médico-social'),
('Services de Maintien à Domicile pour Personnes Âgées',        'Santé et social',                             'Etablissement médico-social pour personnes âgées'),
('Etablissements de l''Aide Sociale à l''Enfance',              'Santé et social',                             'Autre service médico-social'),
('Etablissements de PMI et de Planification Familiale',         'Santé et social',                             'Autre service médico-social'),
('Autres Etablissements médico-sociaux',                        'Santé et social',                             'Autre service médico-social'),
('Logements en Structure Collective',                           'Santé et social',                             'Autre service médico-social'),
('Protection des majeurs',                                      'Santé et social',                             'Autre service médico-social'),
('Autres Etablissements Sociaux d''Hébergement et d''Accueil',  'Santé et social',                             'Autre service médico-social'),
('Etab. Expérimentaux en Faveur de l''Enfance Protégée',        'Santé et social',                             'Autre service médico-social'),
('Etab.et Services du Ministère de la Justice pour Mineurs',    'Santé et social',                             'Autre service médico-social'),
('Centres prestataires de services pr personnes cérébro-lésées','Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etab. Expérimentaux en Faveur de l''Enfance Handicapée',      'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etablissements Expérimentaux en Faveur des Personnes Âgées',  'Santé et social',                             'Etablissement médico-social pour personnes âgées'),
('Etablissements Expérimentaux en Faveur des Adultes',          'Santé et social',                             'Autre service médico-social'),
('Etablissements ou Services Divers d''Aide à la Famille',      'Santé et social',                             'Autre service médico-social'),
('Etablissements et Services Hébergement Enfants Handicapés',   'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Services Sociaux en Faveur des Personnes Âgées',              'Santé et social',                             'Etablissement médico-social pour personnes âgées'),
('Etab.et Services de Réinsertion Prof pour Adultes Handicapés','Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etablissements Expérimentaux en Santé',                       'Santé et social',                             'Autre établissement et service à caractère sanitaire'),
('Commerce de Biens à Usage Médicaux',                          'Santé et social',                             'Autre établissement et service à caractère sanitaire'),
('Autres structures d''exercice libéral',                       'Santé et social',                             'Autre établissement et service à caractère sanitaire'),
('Etablissements d''Education Spéciale pour Handicapés Moteurs','Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etablissements de Formation des Personnels Sociaux',          'Service pour l''emploi et la formation',      'Centre de formation'),
('Etab.Educ.Spéciale pour Déficients Mentaux et Handicapés',    'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etablissements de Formation des Personnels Sanitaires',       'Service pour l''emploi et la formation',      'Centre de formation'),
('Etablissements de Formation Polyvalente',                     'Service pour l''emploi et la formation',      'Centre de formation'),
('Etab.Educ Spéciale pour Enfants Trouble Conduite et Comport', 'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Etab.Education Spéciale pour Déficients Sensoriels',          'Santé et social',                             'Etablissement médico-social pour personnes handicapées'),
('Centres de ressources',                                       'Santé et social',                             'Autre service médico-social');

DROP TABLE IF EXISTS mssfiness2024_national;
CREATE TEMP TABLE mssfiness2024_national AS
WITH finess_query AS (
    SELECT 
        CASE WHEN length(a.nofinesset::VARCHAR)=8 THEN concat('0',a.nofinesset::VARCHAR) ELSE a.nofinesset::VARCHAR END as nofinesset, -- Numéro FINESS
        -- ajout du numero de l'entité juridique
        CASE WHEN length(a.nofinessej::VARCHAR)=8 THEN concat('0',a.nofinessej::VARCHAR) ELSE a.nofinessej::VARCHAR END numero_finess_entite_juridique,
        ----
        CASE
            WHEN LENGTH(a.departement::VARCHAR)=1 THEN LPAD(a.departement::VARCHAR, 2, '0')
            ELSE a.departement
        END AS code_departement_insee,
        CASE
            WHEN a.commune <= 99 THEN LPAD(a.commune::VARCHAR, 3, '0')
            ELSE a.commune::VARCHAR
        END AS code_commune_insee_partial,
        TRIM(
                regexp_replace(
                    CONCAT(a.numvoie,' ',a.typvoie,' ',a.voie::TEXT),
                '\s+',' ','g')::TEXT
                ) AS adresse,
        --CONCAT(CAST(a.'7' AS VARCHAR), ' ', a.'8', ' ', a.'9', ' ', a.'10') AS adresse, -- Numéro de voie + Type de voie  + Libellé de voie + Complément de voie
        a.lieuditbp as lieudit_bp,
        SUBSTRING(a.ligneacheminement FROM 6) as nom_commune,
        SUBSTRING(a.ligneacheminement FROM 1 FOR 5) as code_postal,
        -------
        -- traitement des catégories
        CASE
            WHEN LEFT(a.categagretab::TEXT,1)='1' THEN 'Etablissements Relevant de la Loi Hospitalière'
            WHEN LEFT(a.categagretab::TEXT,1)='2' THEN 'Autres Etablissements de Soins et Prévention'
            WHEN LEFT(a.categagretab::TEXT,1)='3' THEN 'Autres Etablissements à Caractère Sznitaire'
            WHEN LEFT(a.categagretab::TEXT,1)='4' THEN 'Etab.Serv.Soc. d''Accueil Hébergement Assistance Réadaptation'
            WHEN LEFT(a.categagretab::TEXT,1)='6' THEN 'Etab. de Formation des Personnels Sanitaires et Sociaux'
            ELSE 'Autre' end as categorie_fonctionnelle_primaire,

        CASE
            WHEN LEFT(a.categagretab::TEXT,2)='11' THEN 'Etablissements Hospitaliers'
            WHEN LEFT(a.categagretab::TEXT,2)='12' THEN 'Autres Etablissements Relevant de la Loi Hospitalière'
            WHEN LEFT(a.categagretab::TEXT,2)='21' THEN 'Cabinets Libéraux'
            WHEN LEFT(a.categagretab::TEXT,2)='22' THEN 'Autres Etablissements de Sins et Prévention'
            WHEN LEFT(a.categagretab::TEXT,2)='31' THEN 'Laboratoires de Biologie Médicale'
            WHEN LEFT(a.categagretab::TEXT,2)='32' THEN 'Commerce de Biens à Usage Médicaux'
            WHEN LEFT(a.categagretab::TEXT,2)='34' THEN 'Autres Etablissements à Carcatère Sanitaire'

            WHEN LEFT(a.categagretab::TEXT,2)='41' THEN 'Etab. et Serv. pour l''Enfance et la Jeunnesse Handicapé'
            WHEN LEFT(a.categagretab::TEXT,2)='43' THEN 'Etablissemens et Services pour Adultes Handicapés'
            WHEN LEFT(a.categagretab::TEXT,2)='44' THEN 'Etablissements et Services pour Persnnes Agées'
            WHEN LEFT(a.categagretab::TEXT,2)='45' THEN 'Etab. et Serv. Sociaux Concurant à la Protection de l''Enfance'
            WHEN LEFT(a.categagretab::TEXT,2)='46' THEN 'Autres Etab. Accueil, Hébergement, Réadaptation et Services'

            WHEN LEFT(a.categagretab::TEXT,2)='61' THEN 'Etablissements de Formation des Personnels Sanitaires'
            WHEN LEFT(a.categagretab::TEXT,2)='62' THEN 'Etablissements de Formation des Personnels Sociaux'
            WHEN LEFT(a.categagretab::TEXT,2)='63' THEN 'Etablissments de Formation Polyvalente'
            ELSE 'Autre' end as categorie_fonctionnelle_secondaire,

        a.categagretab::TEXT as categorie_aggregat_etablissement,
        a.libcategagretab as libelle_categorie_agregat_etablissement,

        a.categetab::TEXT as categorie_etablissement,
        a.libcategetab as libelle_categorie_etablissement,


        -- catégories join et traitées par @Medi et @Camille
        --'Etablissement public' as categorie_1,
        CASE
            WHEN cm.categorie_2 IS NOT NULL THEN cm.categorie_2
            ELSE 'Autre'
        END AS categorie_2,
        cm.categorie_3,

        -----
        SUBSTRING(a.siret::VARCHAR, 1, 9) as siren,
        a.siret::VARCHAR AS siret,
        CASE
            WHEN a.rslongue IS NOT NULL THEN a.rslongue-- Raison sociale longue
            ELSE a.rs-- Raison sociale
        END AS raison_sociale,
        a.complrs as complement_raison_sociale,
        a.compldistrib as complement_distribution,
        a.codeape as code_activite_principale,
        ----
        a.codemft::TEXT  as code_mode_fixation_tarifaire,
        a.libmft as libelle_mode_fixation_tarifaire,
        a.codesph::TEXT as code_service_public_hospitalier, -- code participation_service_public_hospitalier
        a.libsph as libelle_service_public_hospitalier,
        ----

        a.dateouv::DATE AS date_ouverture,
        a.dateautor::DATE AS date_autorisation,
        a.datemaj::DATE as date_mise_a_jour,
        ----
        CASE
            WHEN a.codesph::TEXT IN ('1','2','3','4') THEN TRUE -- Libelle SPH
            ELSE FALSE
        END AS service_public,
        --

        ST_SetSRID(ST_MakePoint(a.coordxet::float, a.coordyet::float), 2154) as geom_finess

    FROM
        (SELECT *
         FROM mssfiness2024.finess
         WHERE
            CASE
            WHEN LENGTH(departement::VARCHAR)=1 THEN LPAD(departement::VARCHAR, 2, '0')
            ELSE departement
            END = '01'
         )  a
        LEFT JOIN categorie_mapping cm
        ON a.libcategagretab = cm.groupe_categorie
)

SELECT
    nofinesset,
    numero_finess_entite_juridique,
    code_departement_insee,
    CONCAT(code_departement_insee, code_commune_insee_partial) AS code_commune_insee,
    adresse,
    lieudit_bp,
    nom_commune,
    code_postal,
    categorie_fonctionnelle_primaire,
    categorie_fonctionnelle_secondaire,
    categorie_aggregat_etablissement,
    libelle_categorie_agregat_etablissement,
    categorie_etablissement,
    libelle_categorie_etablissement,
    CASE WHEN service_public=TRUE THEN 'Etablissement public' END AS categorie_1,
    categorie_2,
    categorie_3,
    siren,
    siret,
    raison_sociale,
    complement_raison_sociale,
    complement_distribution,
    code_activite_principale,
    ----
    code_mode_fixation_tarifaire,
    libelle_mode_fixation_tarifaire,
    code_service_public_hospitalier, -- code participation_service_public_hospitalier
    libelle_service_public_hospitalier,
    ----
    date_ouverture,
    date_autorisation,
    date_mise_a_jour,
    ----
    service_public,
    --
    geom_finess
FROM finess_query;
CREATE INDEX mssfiness2024_national_nofinesset_index ON mssfiness2024_national USING BTREE (nofinesset);
CREATE INDEX mssfiness2024_national_code_departement_insee_index ON mssfiness2024_national USING BTREE (code_departement_insee);
-----------------------------------------------------------
DROP TABLE IF EXISTS rel_identifiant;
CREATE TEMP TABLE rel_identifiant AS 

SELECT
    nd.nofinesset,
    nd.siret,
    batiment_groupe_id,
    nd.code_departement_insee,
    'adresse du siret' AS methode_appariement,
    recovery_score*0.9 AS score_fiabilite

FROM mssfiness2024_national nd

INNER JOIN rel_batiment_groupe_siret s
    ON nd.code_departement_insee = s.code_departement_insee
    AND nd.siret = s.siret

INNER JOIN geocodage_etaban202203.inssir202308_ban p
    ON nd.code_departement_insee = p.code_departement_insee
    AND nd.siret = p.id_source

WHERE recovery_score >= 0.8
AND etat_administratif_actif;
CREATE INDEX tmp_rel_identifiant_nofinesset_index ON rel_identifiant USING BTREE (nofinesset);
CREATE INDEX tmp_rel_identifiant_batiment_groupe_id_index ON rel_identifiant USING BTREE (batiment_groupe_id);
CREATE INDEX tmp_rel_identifiant_code_departement_insee_index ON rel_identifiant USING BTREE (code_departement_insee);
-----------------------------------------------------------
DROP TABLE IF EXISTS rel_geocodage;
CREATE TEMP TABLE rel_geocodage AS

SELECT
    geoc.id_source AS nofinesset,
    bg_ad.batiment_groupe_id,
    geoc.code_departement_insee,
    'adresse' AS methode_appariement,
    geoc.recovery_score*0.9::numeric AS score_fiabilite

FROM geocodage_etaban202211.mssfiness2024_ban geoc

INNER JOIN rel_batiment_groupe_adresse bg_ad
    ON geoc.code_departement_insee = bg_ad.code_departement_insee
    AND geoc.cle_interop = bg_ad.cle_interop_adr

WHERE recovery_score >= 0.8;
CREATE INDEX tmp_rel_geocodage_nofinesset_index ON rel_geocodage USING BTREE (nofinesset);
CREATE INDEX tmp_rel_geocodage_batiment_groupe_id_index ON rel_geocodage USING BTREE (batiment_groupe_id);
CREATE INDEX tmp_rel_geocodage_code_departement_insee_index ON rel_geocodage USING BTREE (code_departement_insee);
-----------------------------------------------------------
DROP TABLE IF EXISTS rel_geospatial;
CREATE TEMPORARY TABLE rel_geospatial AS

WITH initial_table AS
(
    SELECT

        nofinesset,
        geom_finess,
        code_departement_insee

    FROM mssfiness2024_national
),

intersects_geom_batiment_groupe AS
(
    SELECT

        nd.nofinesset,
        s.batiment_groupe_id,
        nd.code_departement_insee,
        TRUE AS is_intersects_geom_batiment_groupe,
        0.6 AS score_fiabilite

    FROM initial_table nd

    INNER JOIN batiment_groupe s
        ON nd.code_departement_insee = s.code_departement_insee
        AND nd.geom_finess && s.geom_groupe
        AND ST_Intersects(s.geom_groupe, nd.geom_finess)

    WHERE s.batiment_groupe_id IS NOT NULL
    AND NOT s.contient_fictive_geom_groupe
),

intersects_geom_tup AS
(
    SELECT

        nd.nofinesset,
        bg.batiment_groupe_id,
        nd.code_departement_insee,
        TRUE AS is_intersects_geom_tup,
        0.6 AS score_fiabilite

    FROM initial_table nd

    INNER JOIN parcelle_unifiee s
        ON nd.code_departement_insee = s.code_departement_insee
        AND s.geom_parcelle_unifiee && nd.geom_finess
        AND ST_Intersects(s.geom_parcelle_unifiee, nd.geom_finess)

    INNER JOIN batiment_groupe bg
        ON s.code_departement_insee = bg.code_departement_insee
        AND s.parcelle_unifiee_id = bg.parcelle_unifiee_id
        AND bg.batiment_groupe_id IS NOT NULL

    WHERE nd.nofinesset NOT IN (SELECT nofinesset FROM intersects_geom_batiment_groupe)
),

close_tup AS
(
    SELECT

        nd.nofinesset,
        bg.batiment_groupe_id,
        nd.code_departement_insee,
        TRUE AS is_closest_geom_tup,
        COALESCE(2-EXP(0.01386*ST_Distance(s.geom_parcelle_unifiee, nd.geom_finess)), 0)*0.5::NUMERIC AS score_fiabilite,
        ST_Distance(s.geom_parcelle_unifiee, nd.geom_finess) AS distance_tup

    FROM initial_table nd

    INNER JOIN parcelle_unifiee s
        ON nd.code_departement_insee = s.code_departement_insee
        AND ST_DWithin(s.geom_parcelle_unifiee, nd.geom_finess, 50)

    INNER JOIN batiment_groupe bg
        ON s.code_departement_insee = bg.code_departement_insee
        AND s.parcelle_unifiee_id = bg.parcelle_unifiee_id
        AND bg.batiment_groupe_id IS NOT NULL

    WHERE nd.nofinesset NOT IN (SELECT nofinesset FROM intersects_geom_batiment_groupe 
                            UNION ALL
                            SELECT nofinesset FROM intersects_geom_tup)                                         
),

select_closest_tup AS
(
    SELECT

        nofinesset,
        batiment_groupe_id,
        code_departement_insee,
        is_closest_geom_tup,
        score_fiabilite,
        row_number() OVER (PARTITION BY nofinesset ORDER BY distance_tup ASC) AS row_number

    FROM close_tup   i
),

closest_tup AS
(
    SELECT

        nofinesset,
        batiment_groupe_id,
        code_departement_insee,
        is_closest_geom_tup,
        score_fiabilite

    FROM select_closest_tup

    WHERE row_number = 1
),

gather AS
(
    SELECT

        COALESCE(i.nofinesset, w.nofinesset, c.nofinesset) AS nofinesset,
        COALESCE(i.batiment_groupe_id, w.batiment_groupe_id, c.batiment_groupe_id) AS batiment_groupe_id,
        COALESCE(i.code_departement_insee, w.code_departement_insee, c.code_departement_insee) AS code_departement_insee,
        CASE 
            WHEN is_intersects_geom_batiment_groupe THEN 'geom_batiment_groupe'
            WHEN is_intersects_geom_tup THEN 'geom_tup'
            WHEN is_closest_geom_tup THEN 'proche_geom_tup'
        END AS methode_appariement,
        COALESCE(i.score_fiabilite, w.score_fiabilite, c.score_fiabilite) AS score_fiabilite

    FROM intersects_geom_batiment_groupe  i

    FULL OUTER JOIN intersects_geom_tup  w
        ON i.code_departement_insee = w.code_departement_insee
        AND i.nofinesset = w.nofinesset

    FULL OUTER JOIN closest_tup c
        ON i.code_departement_insee = c.code_departement_insee
        AND i.nofinesset = c.nofinesset
)

SELECT

    nofinesset,
    i.batiment_groupe_id,
    i.code_departement_insee,
    methode_appariement,
    score_fiabilite

FROM gather i;
CREATE INDEX tmp_rel_geospatial_nofinesset_index ON rel_geospatial USING BTREE (nofinesset);
CREATE INDEX tmp_rel_geospatial_batiment_groupe_id_index ON rel_geospatial USING BTREE (batiment_groupe_id);
CREATE INDEX tmp_rel_geospatial_code_departement_insee_index ON rel_geospatial USING BTREE (code_departement_insee);
-----------------------------------------------------------
DROP TABLE IF EXISTS rel_batiment_groupe_finess;
CREATE TEMPORARY TABLE rel_batiment_groupe_finess AS

WITH regroupement AS
(
    SELECT

        nofinesset,
        parcelle_unifiee_id,
        s.code_departement_insee,
        methode_appariement,
        score_fiabilite

    FROM rel_identifiant s

    INNER JOIN batiment_groupe p
        ON s.code_departement_insee = p.code_departement_insee
        AND s.batiment_groupe_id = p.batiment_groupe_id

    GROUP BY nofinesset, parcelle_unifiee_id, s.code_departement_insee, methode_appariement, score_fiabilite

    UNION ALL

    SELECT

        nofinesset,
        parcelle_unifiee_id,
        s.code_departement_insee,
        methode_appariement,
        score_fiabilite

    FROM rel_geocodage s

    INNER JOIN batiment_groupe p
        ON s.code_departement_insee = p.code_departement_insee
        AND s.batiment_groupe_id = p.batiment_groupe_id

    GROUP BY nofinesset, parcelle_unifiee_id, s.code_departement_insee, methode_appariement, score_fiabilite

    UNION ALL

    SELECT
        nofinesset,
        parcelle_unifiee_id,
        s.code_departement_insee,
        methode_appariement,
        score_fiabilite

    FROM rel_geospatial s

    INNER JOIN batiment_groupe p
        ON s.code_departement_insee = p.code_departement_insee
        AND s.batiment_groupe_id = p.batiment_groupe_id

    GROUP BY nofinesset, parcelle_unifiee_id, s.code_departement_insee, methode_appariement, score_fiabilite

),

resultat_multiple_methode AS
(
    SELECT

        nofinesset,
        parcelle_unifiee_id,
        r.code_departement_insee,
        STRING_AGG(DISTINCT methode_appariement, ' + ' ORDER BY methode_appariement) AS methode_appariement,
        LEAST(MAX(score_fiabilite) + (COUNT(*)-1)*0.1, 1) AS score_fiabilite

    FROM regroupement r

    GROUP BY nofinesset, parcelle_unifiee_id, r.code_departement_insee
),

ordre_fiabilite AS
(
    SELECT

        *,
        rank() OVER (PARTITION BY nofinesset ORDER BY score_fiabilite DESC, parcelle_unifiee_id ASC) AS ordre_fiab

    FROM resultat_multiple_methode
)

SELECT

    r.nofinesset,
    batiment_groupe_id,
    r.code_departement_insee,
    methode_appariement,
    CASE
        WHEN score_fiabilite = 1 THEN 'Très fiable'
        WHEN score_fiabilite >= 0.82 THEN 'Fiable'
        WHEN score_fiabilite >= 0.72 THEN 'Assez fiable'
        WHEN score_fiabilite >= 0.5 THEN 'Peu fiable'
        ELSE 'Très peu fiable'
    END AS score_fiabilite

FROM ordre_fiabilite r

INNER JOIN bdnb_v07.batiment_groupe bg
    ON r.code_departement_insee = bg.code_departement_insee
    AND r.parcelle_unifiee_id = bg.parcelle_unifiee_id

WHERE ordre_fiab = 1;
CREATE INDEX rel_batiment_groupe_finess_nofinesset_index ON rel_batiment_groupe_finess USING BTREE (nofinesset);
CREATE INDEX rel_batiment_groupe_finess_batiment_groupe_id_index ON rel_batiment_groupe_finess USING BTREE (batiment_groupe_id);
CREATE INDEX rel_batiment_groupe_finess_code_departement_insee_index ON rel_geosprel_batiment_groupe_finessatial USING BTREE (code_departement_insee);
-----------------------------------------------------------
DROP TABLE IF EXISTS coriandre_finess;
CREATE TABLE coriandre_finess AS

SELECT 

    CONCAT('finess_', ROW_NUMBER() OVER (ORDER BY s.nofinesset)) AS id,
	s.nofinesset AS id_coriandre,
	categorie_3 AS categorie_coriandre,
	'finess' AS source,
	s.nofinesset AS id_source,
	s.libelle_categorie_etablissement AS libelle,
	ST_X(geom_finess) AS x_brut,
    ST_Y(geom_finess) AS y_brut,
    NULL AS idpar_brut,
    CONCAT(adresse, ', ', code_postal, ' ', nom_commune) AS adresse_brut,
    siret AS siret_brut,
    CASE
        WHEN methode_appariement LIKE '%geom%' THEN  ST_X(ST_PointOnSurface(ST_Union(geom_groupe)))
    END AS x_fiab,
    CASE
        WHEN methode_appariement LIKE '%geom%' THEN ST_Y(ST_PointOnSurface(ST_Union(geom_groupe)))
    END AS y_fiab,
    CASE
        WHEN methode_appariement LIKE '%geom%' THEN ST_Union(geom_groupe)
    END AS geomloc_fiab,
    NULL AS idpar_fiab,
    CASE
        WHEN methode_appariement LIKE '%adresse%' THEN address_geocoded
    END AS adresse_fiab,
    NULL AS geompar_fiab,
    CASE
        WHEN score_fiabilite = 'Très peu Fiable' THEN 1
        WHEN score_fiabilite = 'Peu fiable' THEN 2
        WHEN score_fiabilite = 'Assez Fiable' THEN 3
        WHEN score_fiabilite = 'Fiable' THEN 4
        WHEN score_fiabilite = 'Très fiable' THEN 5
    END AS niveau_fiab,
    array_agg(rel.batiment_groupe_id) AS liste_batiment_groupe_id,
    'millesime_2023_01_acacia' AS version_bdnb,
	array_agg(bg.parcelle_unifiee_id) AS liste_idtup,
	'2021' AS version_ff,
	array_agg(batiment_construction_id) AS bdtopo_bat_cleaps,
    '2022-09-15' AS version_bdtopo,
    ARRAY_AGG(DISTINCT ARRAY_TO_STRING(l_usage_1, ',')) AS cat_bati_bdtopo,
    ARRAY_AGG(DISTINCT l_usage_niveau_1_txt) AS cat_proprio_type  
    
FROM mssfiness2024_national s

LEFT JOIN rel_batiment_groupe_finess rel
	ON s.code_departement_insee = rel.code_departement_insee 
	AND s.nofinesset = rel.nofinesset

LEFT JOIN batiment_groupe bg
	ON s.code_departement_insee = bg.code_departement_insee 
	AND rel.batiment_groupe_id = bg.batiment_groupe_id
	
LEFT JOIN mssfiness2024_ban adm_ban
	ON s.code_departement_insee = adm_ban.code_departement_insee 
	AND s.nofinesset = adm_ban.id_source
	
LEFT JOIN batiment_construction cons
    ON s.code_departement_insee = cons.code_departement_insee 
    AND rel.batiment_groupe_id = cons.batiment_groupe_id
	
LEFT JOIN batiment_groupe_bdtopo_bat ign
    ON s.code_departement_insee = ign.code_departement_insee
    AND rel.batiment_groupe_id = ign.batiment_groupe_id
	
LEFT JOIN batiment_groupe_ffo_bat ffo
    ON s.code_departement_insee = ffo.code_departement_insee
    AND rel.batiment_groupe_id = ffo.batiment_groupe_id

GROUP BY s.nofinesset, categorie_3, libelle_categorie_etablissement, geom_finess, adresse, nom_commune, siret, methode_appariement, score_fiabilite, address_geocoded;