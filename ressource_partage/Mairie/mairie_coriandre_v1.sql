## INITIALISATION_TABLE_FINALE
--Creation de la table finale dans laquelle seront regroupées toutes les mairies de tous les départements
drop table if exists coriandre.mairie_final;
create table coriandre.mairie_final
(categorie_coriandre varchar,
"source" varchar,
id_source varchar,
insee_com varchar,
libelle varchar,
x_brut float,
y_brut float,
geomloc_fiab geometry(point, 4326),
geomtup geometry(multipolygon, 2154),
geometrie geometry(geometryz, 4326),
version_ff int,
version_bdtopo int,
version_bdnb int,
idtup varchar,
niveau_fiab int,
nlocal int,
l_batiment_groupe_id_tup varchar,
batiment_groupe_id varchar,
cat_bati_bdtopo varchar,
bdtopo_bat_cleabs varchar,
cat_proprio_type text
);



## MAIRIE
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------MAIRIES----------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------

--On utilise la couche commune_chef_lieu de la base ADMIN EXPRESS 2022 qui recense l'ensemble des mairies de la métropole

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les points ADMIN EXPRESS et les TUP des FF 2023
-- On récupère un idtup, le catpro3 (qui devrait être égal à P5a) et on rappatrie le nombre de locaux concerné dans les FF (nlocal)
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_1;
create table coriandre.mairie1_1 as
select
	mairie.nom as libelle,
	mairie.id as id_source,
	mairie.insee_com as insee_com,
	st_transform(mairie.geom,4326) as geomloc_fiab,
	st_x(geom) as x_brut,
	st_y(geom) as y_brut,
	tup.idtup as idtup, 
	tup.catpro3 as cat_proprio_type,
	tup.geomtup,
	tup.nlocal
from (select * from coriandre.mairie_admin_express where insee_com like '{0}%') as mairie
left join ff_annexes_tup_2023.d{0}_2023_tup as tup
on st_intersects(tup.geomtup, st_transform(mairie.geom,2154));

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les mairies et TUP avec un tampon de 5m pour recupérer les TUP de type %P% les plus proches du point
-- On récupère un idtup, le catpro3 (qui devrait être égal à P5a) et on rappatrie le nombre de locaux concerné dans les FF (nlocal)
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_2;
create table coriandre.mairie1_2 as
select mairie.*,
	case when tup.catpro3 like '%P%' then tup.idtup
	when tup.catpro3 not like '%P%' then null
	end as idtup, 
	case when tup.catpro3 like '%P%' then tup.catpro3
	when tup.catpro3 not like '%P%' then null
	end as cat_proprio_type,
	case when tup.catpro3 like '%P%' then tup.geomtup
	when tup.catpro3 not like '%P%' then null
	end as geomtup,
	case when tup.catpro3 like '%P%' then tup.nlocal
	when tup.catpro3 not like '%P%' then null
	end as nlocal,
	st_distance(tup.geomtup, st_transform(mairie.geomloc_fiab,2154)) as distance
from (select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab from coriandre.mairie1_1 where geomtup is null) as mairie
left join ff_annexes_tup_2023.d{0}_2023_tup as tup
on st_dwithin(tup.geomtup, st_transform(mairie.geomloc_fiab,2154), 5);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On rassemble dans la même table kes intersections direct, les tampons de 5m avec les TUP de type %P% et celles qui n'ont aucune intersection
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_3;
create table coriandre.mairie1_3 as
with ensemble_mairie as (select 
					mairie.nom as libelle,
					mairie.id as id_source,
					mairie.insee_com as insee_com,
					st_transform(mairie.geom,4326) as geomloc_fiab,
					st_x(geom)::float as x_brut,
					st_y(geom)::float as y_brut
					from (select * from coriandre.mairie_admin_express where insee_com like '{0}%') mairie)				
select coalesce(ensemble_mairie.libelle, a.libelle) as libelle,
    	coalesce(ensemble_mairie.x_brut, a.x_brut) as x_brut, 
    	coalesce(ensemble_mairie.y_brut, a.y_brut) as y_brut, 
    	coalesce(ensemble_mairie.id_source, a.id_source) as id_source, 
		coalesce(ensemble_mairie.insee_com, a.insee_com) as insee_com,
    	coalesce(ensemble_mairie.geomloc_fiab,a.geomloc_fiab) as geomloc_fiab,
    	a.idtup, a.cat_proprio_type,a.geomtup, a.nlocal
from ((select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab, idtup, cat_proprio_type, geomtup, nlocal
	   from (select *, ROW_NUMBER() OVER (PARTITION BY id_source ORDER BY distance) AS rn
	    	 FROM coriandre.mairie1_2 where cat_proprio_type like '%P%') as t
			 WHERE t.rn = 1)
	union 
	(select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab, idtup, cat_proprio_type, geomtup, nlocal from coriandre.mairie1_1 where geomtup is not null)) a
right join ensemble_mairie 
on ensemble_mairie.id_source = a.id_source;

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les points ADMIN EXPRESS et les batis de la BDTopo 2023
-- On recupère les identifiants batiments bdtopo_cleabs
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1;
create table coriandre.mairie1 as
select 
	mairie.*,
	bdtopo.cleabs as bdtopo_bat_cleabs,
	bdtopo.usage_1 as cat_bati_bdtopo,
	bdtopo.geometrie
from coriandre.mairie1_3 as mairie
left join bdtopo_2023_fr.batiment bdtopo
on st_intersects(bdtopo.geometrie,mairie.geomloc_fiab);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure attributaire entre l'identifiant cleabs de la bdtopo à la BDNB
-- On recupère la liste des bâtiments groupe
-- Attention à la correspondance de millésime
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie2;
create table coriandre.mairie2 as
with corresp_bdnb as
(
select 
	mairie1.id_source,
	array_remove(array_agg(distinct bdnb.batiment_groupe_id), null) as batiment_groupe_id
from coriandre.mairie1 as mairie1
left join bdnb_2023_01_a_extract_cerema_core_bdnb_france.rel_batiment_groupe_bdtopo_bat as bdnb
on mairie1.bdtopo_bat_cleabs=bdnb.bdtopo_bat_cleabs
group by mairie1.id_source
)
select *
from coriandre.mairie1 
join corresp_bdnb 
using(id_source);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure attributaire entre l'idtup et la BDNDB
-- On recupère la liste des bâtiments groupe
-- Attention à la correspondance de millésime
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie3;
create table coriandre.mairie3 as
with corresp as
(
select 
	mairie2.id_source,
	array_remove(array_agg(distinct b.batiment_groupe_id), null) as l_batiment_groupe_id_tup
from coriandre.mairie2  as mairie2
left join bdnb_2023_01_a_extract_cerema_core_bdnb_france.batiment_groupe b
on mairie2.idtup = b.parcelle_unifiee_id
group by mairie2.id_source 
)
select * from coriandre.mairie2 
join corresp 
using(id_source);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- Note de fiabilité :
-- 5 : Si catpro de la TUP est de type P5a
--	   Si 1 seul local FF ou 1 seul bati bdtopo
-- 4 : Si catpro de la TUP est de type P5a
--     Si 1 ou plusieurs batiment_groupe  dans les rattachement à la bdtopo ou à la tup
-- 3 : Si pas de TUP
--     Si 1 seul bati bdtopo
-- 2 : Si  catpro de la TUP est de type P5a mais pas d'information batimentaire (ni bdtopo ni bdnb)
--     attention, ce cas est sans doute lié à des incompatibilité de millésime (surtout si nlocal > 0)
-- 1 : Si catpro de la TUP n'est pas de type P5a
-- 0 : Le reste ?
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie4;
create table coriandre.mairie4 as
select * ,
	case when cat_proprio_type like'%P%' and (nlocal =1 or bdtopo_bat_cleabs notnull) then 5
	when cat_proprio_type like'%P%' and (array_length(batiment_groupe_id,1)>=1 or array_length(l_batiment_groupe_id_tup,1)>=1) then 4
	when idtup is null and bdtopo_bat_cleabs notnull then 3
	when cat_proprio_type like'%P%' then 2
	when cat_proprio_type not like'%P%' then 1
	else 0
	end as niveau_fiab
from coriandre.mairie3;
----------------------------------------------------------------------------------------------------------------------------------------------------------
--Ajout version, source...
----------------------------------------------------------------------------------------------------------------------------------------------------------
alter table coriandre.mairie4
add column version_bdnb int,
add column version_ff int,
add column version_bdtopo int,
add column source varchar,
add column categorie_coriandre varchar;

UPDATE coriandre.mairie4
SET version_bdnb = 2023,
    version_ff = 2023,
    version_bdtopo = 2023,
    source = 'ADMIN EXPRESS',
    categorie_coriandre = 'Mairie';

## CONCATENATION
INSERT INTO coriandre.mairie_final (
	categorie_coriandre,
	"source",
	id_source,
	insee_com,
	libelle,
	x_brut,
    y_brut,
	geomloc_fiab,
	geomtup,
	geometrie,
	version_ff,
	version_bdtopo,
	version_bdnb,
	idtup,
	niveau_fiab,
	nlocal,
	l_batiment_groupe_id_tup,
	batiment_groupe_id,
	cat_bati_bdtopo,
	bdtopo_bat_cleabs,
	cat_proprio_type)
SELECT 	
	categorie_coriandre,
	"source",
	id_source,
	insee_com,
	libelle,
	x_brut::float8,
    y_brut::float8,
	geomloc_fiab,
	geomtup,
	geometrie,
	version_ff,
	version_bdtopo,
	version_bdnb,
	idtup,
	niveau_fiab,
	nlocal,
	l_batiment_groupe_id_tup,
	batiment_groupe_id,
	cat_bati_bdtopo,
	bdtopo_bat_cleabs,
	cat_proprio_type
FROM 
    coriandre.mairie4;


## CORSE_2A
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------MAIRIES----------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------

--On utilise la couche commune_chef_lieu de la base ADMIN EXPRESS 2022

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les points ADMIN EXPRESS et les TUP des FF 2023
-- On récupère un idtup, le catpro3 (qui devrait être égal à P5a) et on rappatrie le nombre de locaux concerné dans les FF (nlocal)
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_1;
create table coriandre.mairie1_1 as
select
	mairie.nom as libelle,
	mairie.id as id_source,
	mairie.insee_com as insee_com,
	st_transform(mairie.geom,4326) as geomloc_fiab,
	st_x(geom) as x_brut,
	st_y(geom) as y_brut,
	tup.idtup as idtup, 
	tup.catpro3 as cat_proprio_type,
	tup.geomtup,
	tup.nlocal
from (select * from coriandre.mairie_admin_express where insee_com like '2A%') as mairie
left join ff_annexes_tup_2023.d2a_2023_tup as tup
on st_intersects(tup.geomtup, st_transform(mairie.geom,2154));

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les mairies et TUP avec un tampon de 5m pour recupérer les TUP de type %P% les plus proches du point
-- On récupère un idtup, le catpro3 (qui devrait être égal à P5a) et on rappatrie le nombre de locaux concerné dans les FF (nlocal)
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_2;
create table coriandre.mairie1_2 as
select mairie.*,
	case when tup.catpro3 like '%P%' then tup.idtup
	when tup.catpro3 not like '%P%' then null
	end as idtup, 
	case when tup.catpro3 like '%P%' then tup.catpro3
	when tup.catpro3 not like '%P%' then null
	end as cat_proprio_type,
	case when tup.catpro3 like '%P%' then tup.geomtup
	when tup.catpro3 not like '%P%' then null
	end as geomtup,
	case when tup.catpro3 like '%P%' then tup.nlocal
	when tup.catpro3 not like '%P%' then null
	end as nlocal,
	st_distance(tup.geomtup, st_transform(mairie.geomloc_fiab,2154)) as distance
from (select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab from coriandre.mairie1_1 where geomtup is null) as mairie
left join ff_annexes_tup_2023.d2a_2023_tup as tup
on st_dwithin(tup.geomtup, st_transform(mairie.geomloc_fiab,2154), 5);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On rassemble dans la même table kes intersections direct, les tampons de 5m avec les TUP de type %P% et celles qui n'ont aucune intersection
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_3;
create table coriandre.mairie1_3 as
with ensemble_mairie as (select 
					mairie.nom as libelle,
					mairie.id as id_source,
					mairie.insee_com as insee_com,
					st_transform(mairie.geom,4326) as geomloc_fiab,
					st_x(geom)::float as x_brut,
					st_y(geom)::float as y_brut
					from (select * from coriandre.mairie_admin_express where insee_com like '2A%') mairie)				
select coalesce(ensemble_mairie.libelle, a.libelle) as libelle,
    	coalesce(ensemble_mairie.x_brut, a.x_brut) as x_brut, 
    	coalesce(ensemble_mairie.y_brut, a.y_brut) as y_brut, 
    	coalesce(ensemble_mairie.id_source, a.id_source) as id_source, 
		coalesce(ensemble_mairie.insee_com, a.insee_com) as insee_com,
    	coalesce(ensemble_mairie.geomloc_fiab,a.geomloc_fiab) as geomloc_fiab,
    	a.idtup, a.cat_proprio_type,a.geomtup, a.nlocal
from ((select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab, idtup, cat_proprio_type, geomtup, nlocal
	   from (select *, ROW_NUMBER() OVER (PARTITION BY id_source ORDER BY distance) AS rn
	    	 FROM coriandre.mairie1_2 where cat_proprio_type like '%P%') as t
			 WHERE t.rn = 1)
	union 
	(select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab, idtup, cat_proprio_type, geomtup, nlocal from coriandre.mairie1_1 where geomtup is not null)) a
right join ensemble_mairie 
on ensemble_mairie.id_source = a.id_source;

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les points ADMIN EXPRESS et les batis de la BDTopo 2023
-- On recupère les identifiants batiments bdtopo_cleabs
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1;
create table coriandre.mairie1 as
select 
	mairie.*,
	bdtopo.cleabs as bdtopo_bat_cleabs,
	bdtopo.usage_1 as cat_bati_bdtopo,
	bdtopo.geometrie
from coriandre.mairie1_3 as mairie
left join bdtopo_2023_fr.batiment bdtopo
on st_intersects(bdtopo.geometrie,mairie.geomloc_fiab);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure attributaire entre l'identifiant cleabs de la bdtopo à la BDNB
-- On recupère la liste des bâtiments groupe
-- Attention à la correspondance de millésime
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie2;
create table coriandre.mairie2 as
with corresp_bdnb as
(
select 
	mairie1.id_source,
	array_remove(array_agg(distinct bdnb.batiment_groupe_id), null) as batiment_groupe_id
from coriandre.mairie1 as mairie1
left join bdnb_2023_01_a_extract_cerema_core_bdnb_france.rel_batiment_groupe_bdtopo_bat as bdnb
on mairie1.bdtopo_bat_cleabs=bdnb.bdtopo_bat_cleabs
group by mairie1.id_source
)
select *
from coriandre.mairie1 
join corresp_bdnb 
using(id_source);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure attributaire entre l'idtup et la BDNDB
-- On recupère la liste des bâtiments groupe
-- Attention à la correspondance de millésime
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie3;
create table coriandre.mairie3 as
with corresp as
(
select 
	mairie2.id_source,
	array_remove(array_agg(distinct b.batiment_groupe_id), null) as l_batiment_groupe_id_tup
from coriandre.mairie2  as mairie2
left join bdnb_2023_01_a_extract_cerema_core_bdnb_france.batiment_groupe b
on mairie2.idtup = b.parcelle_unifiee_id
group by mairie2.id_source 
)
select * from coriandre.mairie2 
join corresp 
using(id_source);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- Note de fiabilité :
-- 5 : Si catpro de la TUP est de type P5a
--	   Si 1 seul local FF ou 1 seul bati bdtopo
-- 4 : Si catpro de la TUP est de type P5a
--     Si 1 ou plusieurs batiment_groupe  dans les rattachement à la bdtopo ou à la tup
-- 3 : Si pas de TUP
--     Si 1 seul bati bdtopo
-- 2 : Si  catpro de la TUP est de type P5a mais pas d'information batimentaire (ni bdtopo ni bdnb)
--     attention, ce cas est sans doute lié à des incompatibilité de millésime (surtout si nlocal > 0)
-- 1 : Si catpro de la TUP n'est pas de type P5a
-- 0 : Le reste ?
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie4;
create table coriandre.mairie4 as
select * ,
	case when cat_proprio_type like'%P%' and (nlocal =1 or bdtopo_bat_cleabs notnull) then 5
	when cat_proprio_type like'%P%' and (array_length(batiment_groupe_id,1)>=1 or array_length(l_batiment_groupe_id_tup,1)>=1) then 4
	when idtup is null and bdtopo_bat_cleabs notnull then 3
	when cat_proprio_type like'%P%' then 2
	when cat_proprio_type not like'%P%' then 1
	else 0
	end as niveau_fiab
from coriandre.mairie3;
----------------------------------------------------------------------------------------------------------------------------------------------------------
------------Ajout version, source...
----------------------------------------------------------------------------------------------------------------------------------------------------------
alter table coriandre.mairie4
add column version_bdnb int,
add column version_ff int,
add column version_bdtopo int,
add column source varchar,
add column categorie_coriandre varchar;

UPDATE coriandre.mairie4
SET version_bdnb = 2023,
    version_ff = 2023,
    version_bdtopo = 2023,
    source = 'ADMIN EXPRESS',
    categorie_coriandre = 'Mairie';



## CORSE_2B
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------MAIRIES----------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------

--On utilise la couche commune_chef_lieu de la base ADMIN EXPRESS 2022

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les points ADMIN EXPRESS et les TUP des FF 2023
-- On récupère un idtup, le catpro3 (qui devrait être égal à P5a) et on rappatrie le nombre de locaux concerné dans les FF (nlocal)
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_1;
create table coriandre.mairie1_1 as
select
	mairie.nom as libelle,
	mairie.id as id_source,
	mairie.insee_com as insee_com,
	st_transform(mairie.geom,4326) as geomloc_fiab,
	st_x(geom) as x_brut,
	st_y(geom) as y_brut,
	tup.idtup as idtup, 
	tup.catpro3 as cat_proprio_type,
	tup.geomtup,
	tup.nlocal
from (select * from coriandre.mairie_admin_express where insee_com like '2B%') as mairie
left join ff_annexes_tup_2023.d2b_2023_tup as tup
on st_intersects(tup.geomtup, st_transform(mairie.geom,2154));

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les mairies et TUP avec un tampon de 5m pour recupérer les TUP de type %P% les plus proches du point
-- On récupère un idtup, le catpro3 (qui devrait être égal à P5a) et on rappatrie le nombre de locaux concerné dans les FF (nlocal)
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_2;
create table coriandre.mairie1_2 as
select mairie.*,
	case when tup.catpro3 like '%P%' then tup.idtup
	when tup.catpro3 not like '%P%' then null
	end as idtup, 
	case when tup.catpro3 like '%P%' then tup.catpro3
	when tup.catpro3 not like '%P%' then null
	end as cat_proprio_type,
	case when tup.catpro3 like '%P%' then tup.geomtup
	when tup.catpro3 not like '%P%' then null
	end as geomtup,
	case when tup.catpro3 like '%P%' then tup.nlocal
	when tup.catpro3 not like '%P%' then null
	end as nlocal,
	st_distance(tup.geomtup, st_transform(mairie.geomloc_fiab,2154)) as distance
from (select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab from coriandre.mairie1_1 where geomtup is null) as mairie
left join ff_annexes_tup_2023.d2b_2023_tup as tup
on st_dwithin(tup.geomtup, st_transform(mairie.geomloc_fiab,2154), 5);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On rassemble dans la même table kes intersections direct, les tampons de 5m avec les TUP de type %P% et celles qui n'ont aucune intersection
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1_3;
create table coriandre.mairie1_3 as
with ensemble_mairie as (select 
					mairie.nom as libelle,
					mairie.id as id_source,
					mairie.insee_com as insee_com,
					st_transform(mairie.geom,4326) as geomloc_fiab,
					st_x(geom)::float as x_brut,
					st_y(geom)::float as y_brut
					from (select * from coriandre.mairie_admin_express where insee_com like '2B%') mairie)				
select coalesce(ensemble_mairie.libelle, a.libelle) as libelle,
    	coalesce(ensemble_mairie.x_brut, a.x_brut) as x_brut, 
    	coalesce(ensemble_mairie.y_brut, a.y_brut) as y_brut, 
    	coalesce(ensemble_mairie.id_source, a.id_source) as id_source,
		coalesce(ensemble_mairie.insee_com, a.insee_com) as insee_com, 
    	coalesce(ensemble_mairie.geomloc_fiab,a.geomloc_fiab) as geomloc_fiab,
    	a.idtup, a.cat_proprio_type,a.geomtup, a.nlocal
from ((select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab, idtup, cat_proprio_type, geomtup, nlocal
	   from (select *, ROW_NUMBER() OVER (PARTITION BY id_source ORDER BY distance) AS rn
	    	 FROM coriandre.mairie1_2 where cat_proprio_type like '%P%') as t
			 WHERE t.rn = 1)
	union 
	(select libelle, x_brut, y_brut, id_source, insee_com, geomloc_fiab, idtup, cat_proprio_type, geomtup, nlocal from coriandre.mairie1_1 where geomtup is not null)) a
right join ensemble_mairie 
on ensemble_mairie.id_source = a.id_source;

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure géométrique entre les points ADMIN EXPRESS et les batis de la BDTopo 2023
-- On recupère les identifiants batiments bdtopo_cleabs
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie1;
create table coriandre.mairie1 as
select 
	mairie.*,
	bdtopo.cleabs as bdtopo_bat_cleabs,
	bdtopo.usage_1 as cat_bati_bdtopo,
	bdtopo.geometrie
from coriandre.mairie1_3 as mairie
left join bdtopo_2023_fr.batiment bdtopo
on st_intersects(bdtopo.geometrie,mairie.geomloc_fiab);
----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure attributaire entre l'identifiant cleabs de la bdtopo à la BDNB
-- On recupère la liste des bâtiments groupe
-- Attention à la correspondance de millésime
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie2;
create table coriandre.mairie2 as
with corresp_bdnb as
(
select 
	mairie1.id_source,
	array_remove(array_agg(distinct bdnb.batiment_groupe_id), null) as batiment_groupe_id
from coriandre.mairie1 as mairie1
left join bdnb_2023_01_a_extract_cerema_core_bdnb_france.rel_batiment_groupe_bdtopo_bat as bdnb
on mairie1.bdtopo_bat_cleabs=bdnb.bdtopo_bat_cleabs
group by mairie1.id_source
)
select *
from coriandre.mairie1 
join corresp_bdnb 
using(id_source);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- On crée une jointure attributaire entre l'idtup et la BDNDB
-- On recupère la liste des bâtiments groupe
-- Attention à la correspondance de millésime
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie3;
create table coriandre.mairie3 as
with corresp as
(
select 
	mairie2.id_source,
	array_remove(array_agg(distinct b.batiment_groupe_id), null) as l_batiment_groupe_id_tup
from coriandre.mairie2  as mairie2
left join bdnb_2023_01_a_extract_cerema_core_bdnb_france.batiment_groupe b
on mairie2.idtup = b.parcelle_unifiee_id
group by mairie2.id_source 
)
select * from coriandre.mairie2 
join corresp 
using(id_source);

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- Note de fiabilité :
-- 5 : Si catpro de la TUP est de type P5a
--	   Si 1 seul local FF ou 1 seul bati bdtopo
-- 4 : Si catpro de la TUP est de type P5a
--     Si 1 ou plusieurs batiment_groupe  dans les rattachement à la bdtopo ou à la tup
-- 3 : Si pas de TUP
--     Si 1 seul bati bdtopo
-- 2 : Si  catpro de la TUP est de type P5a mais pas d'information batimentaire (ni bdtopo ni bdnb)
--     attention, ce cas est sans doute lié à des incompatibilité de millésime (surtout si nlocal > 0)
-- 1 : Si catpro de la TUP n'est pas de type P5a
-- 0 : Le reste ?
----------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists coriandre.mairie4;
create table coriandre.mairie4 as
select * ,
	case when cat_proprio_type like'%P%' and (nlocal =1 or bdtopo_bat_cleabs notnull) then 5
	when cat_proprio_type like'%P%' and (array_length(batiment_groupe_id,1)>=1 or array_length(l_batiment_groupe_id_tup,1)>=1) then 4
	when idtup is null and bdtopo_bat_cleabs notnull then 3
	when cat_proprio_type like'%P%' then 2
	when cat_proprio_type not like'%P%' then 1
	else 0
	end as niveau_fiab
from coriandre.mairie3;
----------------------------------------------------------------------------------------------------------------------------------------------------------
------------Ajout version, source...
----------------------------------------------------------------------------------------------------------------------------------------------------------
alter table coriandre.mairie4
add column version_bdnb int,
add column version_ff int,
add column version_bdtopo int,
add column source varchar,
add column categorie_coriandre varchar;

UPDATE coriandre.mairie4
SET version_bdnb = 2023,
    version_ff = 2023,
    version_bdtopo = 2023,
    source = 'ADMIN EXPRESS',
    categorie_coriandre = 'Mairie';