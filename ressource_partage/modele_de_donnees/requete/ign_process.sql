DROP TABLE IF EXISTS z_tmp_mhamdoune.ign_zai;
CREATE table z_tmp_mhamdoune.ign_zai as (

-- traitement de la table ign_bdtopo_zone_d_activite_ou_d_interet, 

with correct_fictif_ign as (
	select 

		bdtopo_zoa_cleabs,
		code_departement_insee,
		geom_zone,
		fictif,
		id_source,
		"source" as source_ign
	
	from z_dbt__bdnb_2023_11_prep_donnees_mhamdoune.ign_bdtopo_zone_d_activite_ou_d_interet
	where fictif = false 
     	
    union

    -- en cas d'intersection entre deux source, on prend la geom de la source avec fictif = False
    
    select
    
    	ign1.bdtopo_zoa_cleabs,
    	ign1.code_departement_insee,
		case 
			WHEN ign2.geom_zone is null then ign1.geom_zone
			else ign2.geom_zone
		end as geom_zone,
		case 
			WHEN ign2.geom_zone is null then true
			else FALSE
		end as fictif,
		ign1.id_source,
		ign1."source" as source_ign
		
    from (select * from z_dbt__bdnb_2023_11_prep_donnees_mhamdoune.ign_bdtopo_zone_d_activite_ou_d_interet where fictif = true) ign1 
	left join (select * from z_dbt__bdnb_2023_11_prep_donnees_mhamdoune.ign_bdtopo_zone_d_activite_ou_d_interet where fictif = False) ign2
		ON ign1.code_departement_insee = ign2.code_departement_insee
	 	AND ign1.geom_zone && ign2.geom_zone
     	AND ST_Intersects(ign1.geom_zone, ign2.geom_zone)
    
),

coorect_geolocalisant as (

    -- transformer la geom_zone en points RNB
	select 
	

		ced.code_departement_insee,
		ced.geom_espace,
		ced.categorie_2,
		ced.categorie_3,
		ced.id_source_brut,
        id_source,
		ced."source" as source_espace,
		ign.bdtopo_zoa_cleabs,
		ign.geom_zone,
		ign.fictif,
		ign.id_source as id_source_ign,
		ign.source_ign,
		rnb_id,
		geom_rnb_pos as geom_rnb,
		null as geom_groupe
			
	from z_tmp_mhamdoune.joining_sources ced 
	inner join correct_fictif_ign ign
		on ced.id_source_brut = ign.id_source 
		and ced.code_departement_insee = ign.code_departement_insee 
	left join bdnb_v07.rel_batiment_construction_rnb rnb
		 ON rnb.code_departement_insee = ign.code_departement_insee
		 AND rnb.geom_rnb_pos && ign.geom_zone
	     AND ST_Intersects(rnb.geom_rnb_pos, ign.geom_zone)  
	where fictif = false
	
	
	union 
	
	-- transformer la geom_zone  fictive en geom_batiment_groupe puis en points RNB
	select 
	
		ced.code_departement_insee,
		ced.geom_espace,
		ced.categorie_2,
		ced.categorie_3,
		ced.id_source_brut,
        id_source,
		ced."source" as source_espace,
		ign.bdtopo_zoa_cleabs,
		ign.geom_zone,
		ign.fictif,
		ign.id_source as id_source_ign,
		ign.source_ign,
		rnb_id,
		geom_rnb_pos as geom_rnb,
		geom_groupe
			
	from z_tmp_mhamdoune.joining_sources ced 
	inner join correct_fictif_ign ign
		on ced.id_source_brut = ign.id_source 
		and ced.code_departement_insee = ign.code_departement_insee
	left join bdnb_v07.batiment_groupe bg
		 ON bg.code_departement_insee = ign.code_departement_insee
		 AND bg.geom_groupe && ign.geom_zone
	     AND ST_Intersects(bg.geom_groupe, ign.geom_zone)
	left join bdnb_v07.rel_batiment_construction_rnb rnb
		 ON rnb.code_departement_insee = bg.code_departement_insee
		 AND rnb.geom_rnb_pos && bg.geom_groupe
	     AND ST_Intersects(rnb.geom_rnb_pos, bg.geom_groupe)   
	where fictif = True
)


select 

    is_source,
	max(code_departement_insee) as code_departement_insee,
	ST_Union(geom_espace) as geom_espace,
	max(categorie_2) as categorie_2,
	max(categorie_3) as categorie_3,
	max(id_source_brut) as id_source_brut,
	max(source_espace) as source_espace,
	max(bdtopo_zoa_cleabs) as bdtopo_zoa_cleabs,
	ST_Union(geom_zone) as geom_zone,
	BOOL_OR(fictif) as fictif,
	max(id_source) as id_source,
	max(source_ign) as source_ign,
	array_agg(distinct rnb_id) as liste_id_rnb,
	ST_Collect(distinct geom_rnb) as geom_rnb,
	ST_Union(geom_groupe) as geom_groupe
	
from coorect_geolocalisant
group by is_source


);

DROP INDEX IF exists index_code_departement_insee_ign_espace;
DROP INDEX IF exists index_id_detail_ign_espace;
CREATE INDEX index_code_departement_insee_ign_espace ON z_tmp_mhamdoune.ign_zai (code_departement_insee);
CREATE INDEX index_id_detail_ign_espace ON z_tmp_mhamdoune.ign_zai (id_detail);

----------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS z_tmp_mhamdoune.table_detail_ign;
CREATE table z_tmp_mhamdoune.table_detail_ign as (

-- Corriger la table joining sources 
select 

	liste_id_rnb,  
	geom_espace as geometrie_rnb, 
	false as ign,
	null as fictif,
	categorie_1, 
	categorie_2, 
	categorie_3, 
	code_commune_insee, 
	code_departement_insee, 
	score_fiabilite, 
	"source", 
	id_source_brut, 
	id_source_corrige, 
	methode_appariement_retenue, 
	denomination, 
	libelle_adresse_principale, 
	siret_principal, 
	propriete_public_probable, 
	usage_public, 
	categorie_public, 
	erp, 
	categorie_erp

from z_tmp_mhamdoune.joining_sources 
where id_source not in (select id_source from z_tmp_mhamdoune.ign_zai)


union 


select 

	case 
		when ign.geom_rnb is null then jn.liste_id_rnb
		else ign.liste_id_rnb
	end as liste_id_rnb,
	case 
		when ign.geom_rnb is null then jn.geom_espace
		else ign.geom_rnb
	end as geometrie_rnb,
	case 
		when ign.geom_rnb is null then False
		else True
	end as ign,
	ign.fictif,
	jn.categorie_1, 
	ign.categorie_2, 
	ign.categorie_3, 
	jn.code_commune_insee, 
	ign.code_departement_insee, 
	case 
		when ign.geom_rnb is not null and fictif = FALSE then 6 -- si l'id_source est corrigé et que sa géométrie n'est pas fictive --> 6
		when ign.geom_rnb is not null and fictif = TRUE then 5 -- si l'id_source est corrigé et que sa géométrie est fictive --> 5
		else jn.score_fiabilite
	end as score_fiabilite, 
	jn."source", 
	jn.id_source_brut, 
	jn.id_source_corrige, 
	jn.id_source,
	jn.methode_appariement_retenue, 
	jn.denomination, 
	jn.libelle_adresse_principale, 
	jn.siret_principal, 
	jn.propriete_public_probable, 
	jn.usage_public, 
	jn.categorie_public, 
	jn.erp, 
	jn.categorie_erp

from z_tmp_mhamdoune.ign_zai ign
left join z_tmp_mhamdoune.joining_sources jn
on ign.id_source = jn.id_source 
and ign.code_departement_insee = jn.code_departement_insee 

);
