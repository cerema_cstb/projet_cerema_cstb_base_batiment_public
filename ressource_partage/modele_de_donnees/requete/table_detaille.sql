DROP TABLE IF EXISTS "z_tmp_mhamdoune".coriandre_detail;
CREATE TABLE "z_tmp_mhamdoune".coriandre_detail as (

select 
	
	'etab-dt-' || LEFT(MD5(rel.id_synthese || joins.id_source), 10) as id_detail, 
	rel.id_synthese,
	
	rel.liste_id_rnb as liste_id_rnb, 
	rel.geometrie_rnb as geometrie_rnb,
	rel.ign as ign, 
	
	joins.categorie_2,
	joins.categorie_3,
	
	joins.code_commune_insee, 
	joins.code_departement_insee, 
	
	joins.score_fiabilite, 
	
	joins."source" as "source", 
	joins.id_source_brut, 
	joins.id_source_corrige,
	
	joins.methode_appariement_retenue, 
		
	joins.denomination, 
	
	joins.libelle_adresse_principale, 
	joins.siret_principal, 
	
	joins.propriete_public_probable, 
	joins.usage_public, 
	joins.categorie_public, 
	
	joins.erp, 
	joins.categorie_erp
	
	
from "z_tmp_mhamdoune".rel_id_synthese_id_source_ign rel
	left join "z_tmp_mhamdoune".table_detail_ign joins
		on rel.id_source = joins.id_source 
		and rel.code_departement_insee = joins.code_departement_insee 
order by code_departement_insee asc, id_synthese
		
);


CREATE INDEX index_code_departement_insee_table_detail ON "z_tmp_mhamdoune".coriandre_detail (code_departement_insee);
CREATE INDEX index_id_synthese_table_detail ON "z_tmp_mhamdoune".coriandre_detail (id_synthese);

ALTER TABLE "z_tmp_mhamdoune".coriandre_detail
ADD CONSTRAINT pk_id_interne_table_detail PRIMARY KEY (id_detail);
