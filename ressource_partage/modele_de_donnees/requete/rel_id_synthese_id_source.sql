DROP TABLE IF EXISTS "z_tmp_mhamdoune".rel_id_synthese_id_source;
CREATE TABLE "z_tmp_mhamdoune".rel_id_synthese_id_source as (

-- 1 éclater la liste_id_rnb

WITH expanded AS (
  SELECT
    id_source,
    code_departement_insee, 
    categorie_3,
    ign,
    liste_id_rnb, 
    geometrie_rnb,
    UNNEST(liste_id_rnb) AS id_rnb -- Éclater la liste_id_rnb pour travailler sur chaque id_rnb individuellement
  FROM 
    z_tmp_mhamdoune.table_detail_ign
),


duplicates AS (
  SELECT
    id_rnb,
    categorie_3,
    COUNT(DISTINCT id_source) AS source_count
  FROM 
    expanded
  GROUP BY
    id_rnb, categorie_3
  HAVING 
    COUNT(DISTINCT id_source) > 1 -- On cherche des id_rnb communs à plusieurs sources dans la même catégorie
),


-- 2 Créer un numéro unique pour chaque groupe de doublons :
numbered_duplicates AS (
  SELECT
    d.id_rnb,
    d.categorie_3,
    ROW_NUMBER() OVER (ORDER BY d.id_rnb, d.categorie_3) AS doublon_group
  FROM 
    duplicates d
),


-- 3 Associer les groupes de doublons avec les sources originales
grouped AS (
  SELECT
    e.id_source,
    e.code_departement_insee,
    e.categorie_3,
    n.doublon_group,
    e.liste_id_rnb,
    e.geometrie_rnb,
    e.ign
  FROM 
    expanded e
  JOIN 
    numbered_duplicates n
  ON 
    e.id_rnb = n.id_rnb
    AND e.categorie_3 = n.categorie_3
), 


-- grouper les doublon_group by source
double_doublon as (
select 
	id_source,
	max(code_departement_insee) as code_departement_insee,
	max(doublon_group) as doublon_group,
	max(liste_id_rnb) as liste_id_rnb, 
	ST_UNION(geometrie_rnb) as geometrie_rnb,
	bool_or(ign) as ign
from grouped
group by id_source

union 

-- ajouter les id_source qui n'ont pas de doublon
select 
	id_source,
	max(code_departement_insee) as code_departement_insee,
	NULL as doublon_group,
	max(liste_id_rnb) as liste_id_rnb, 
	ST_UNION(geometrie_rnb) as geometrie_rnb,
	bool_or(ign) as ign
from expanded
where id_source not in (select id_source from grouped)
group by id_source

), 



-- group by doublon_group pour creer id_synthese
define_id_synthese as (
select 
	'etab-pu-' || LEFT(MD5(STRING_AGG(DISTINCT id_source, '' ORDER BY id_source)), 10) as id_synthese, 
	max(code_departement_insee) as code_departement_insee,
	COALESCE(MAX(CASE WHEN ign THEN liste_id_rnb ELSE NULL END), MAX(liste_id_rnb)) AS liste_id_rnb, 
	COALESCE(ST_Union(CASE WHEN ign THEN geometrie_rnb ELSE NULL END), ST_Union(geometrie_rnb)) AS geometrie_rnb, 
	bool_or(ign) as ign, 
	array_agg(DISTINCT id_source) AS array_id_source,
	count(DISTINCT id_source) AS nb_id_source
from double_doublon
group by coalesce(doublon_group::text, id_source) -- grouper par doublon_group ou par id_source pour les id qui n'ont pas de doublons
),

-- eclater array_id_source pour creer la table de relation id_source, id_synthese
unnest_id_source as (
SELECT 
	id_synthese, 
	code_departement_insee, 
	liste_id_rnb, 
	geometrie_rnb, 
	ign, 
	nb_id_source, 
	UNNEST(array_id_source) AS id_source 
FROM define_id_synthese	
)




SELECT
		
	rel.id_source,
	id_synthese, 
	"source",
	rel.code_departement_insee, 
	rel.liste_id_rnb, 
	rel.geometrie_rnb, 
	rel.ign
	
FROM unnest_id_source rel
	left join z_tmp_mhamdoune.table_detail_ign j
		on rel.id_source = j.id_source
		and rel.code_departement_insee = j.code_departement_insee


union all -- union avec les id_source qui n'ont pas de liste_id_rnb

select 
	id_source,
	'etab-pu-' || LEFT(MD5(id_source), 10) as id_synthese, 
	"source",
	code_departement_insee,
	liste_id_rnb, 
	geometrie_rnb, 
	ign
	
from z_tmp_mhamdoune.table_detail_ign 
where liste_id_rnb is null

order by code_departement_insee

);


CREATE INDEX index_code_departement_insee_synthese_source ON z_tmp_mhamdoune.rel_id_synthese_id_source (code_departement_insee);
CREATE INDEX index_geom_rnb_synthese_source  ON z_tmp_mhamdoune.rel_id_synthese_id_source USING GIST (geometrie_rnb);

ALTER TABLE z_tmp_mhamdoune.rel_id_synthese_id_source
ADD CONSTRAINT pk_rel_id_synthese_id_source  PRIMARY KEY (id_source, id_synthese);