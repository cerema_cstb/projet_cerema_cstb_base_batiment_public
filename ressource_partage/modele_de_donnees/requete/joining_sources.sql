DROP TABLE IF EXISTS "work".joinining_sources;
CREATE TABLE "work".joinining_sources as (

with groupe_id_tup_id_source as (

SELECT 

	array_remove(liste_id_rnb, NULL) as liste_id_rnb,
    geometrie_rnb,
    'Etablissement public' AS categorie_1,
    categorie_2,
    categorie_3,
    code_commune_insee,
    code_departement_insee,
    score_fiabilite,
    "source",
    id_source_brut, 
    id_source_corrige,
    COALESCE(id_source_corrige, id_source_brut) AS id_source,
    
   	case 
   		when methode_appariement_retenue = 'geom_tup + tampon 5m' then 'proche_geom_tup'
   		else methode_appariement_retenue
   	end as methode_appariement_retenue,
   	
    idtup,
    catpro3,
    denomination,
    libelle_adresse_principale,
    siret_principal,
    propriete_public_probable,
    usage_public,
    categorie_public,
    erp,
    categorie_erp,
    ROW_NUMBER() OVER (PARTITION BY id_source_brut ORDER BY score_fiabilite DESC) as rn

FROM 
    "work".admin_express_basilic_rnb adm


)



SELECT 
	liste_id_rnb, 
	geometrie_rnb, 
	categorie_1, 
	categorie_2, 
	categorie_3, 
	code_commune_insee, 
	code_departement_insee, 
	score_fiabilite, 
	"source", 
	id_source_brut, 
	id_source_corrige, 
	coalesce(id_source_corrige, id_source_brut) as id_source,
	methode_appariement_retenue, 
	idtup, 
	catpro3, 
	denomination, 
	libelle_adresse_principale, 
	siret_principale as siret_principal, 
	propriete_public_probable, 
	usage_public, 
	categorie_public, 
	erp, 
	categorie_erp
FROM "work".etablissement_public_cstb

UNION 

SELECT 
	liste_id_rnb, 
	geometrie_rnb, 
	categorie_1, 
	
	case 
		when categorie_3 = 'bibliothèque et médiathèque' then 'equipement culturel et socioculturel'
		when categorie_3 = 'service d’archives' then 'equipement culturel et socioculturel'
		when categorie_3 = 'éducation et formation' then 'equipement culturel et socioculturel'
		when categorie_3 = 'musée et centre culturel' then 'equipement culturel et socioculturel'
		when categorie_3 = 'arts du spectacle' then 'equipement culturel et socioculturel'
		when categorie_3 = 'mairie' then 'administration'
		when categorie_3 = 'cinéma' then 'equipement culturel et socioculturel'
		else null
	end as categori_2,
	
	case 
		when categorie_3 = 'bibliothèque et médiathèque' then 'bibliothèque et médiathèque'
		when categorie_3 = 'service d’archives' then 'dépôt d''archives'
		when categorie_3 = 'éducation et formation' then 'éducation et formation'
		when categorie_3 = 'musée et centre culturel' then 'musée et centre culturel'
		when categorie_3 = 'arts du spectacle' then 'salle de spectacles'
		when categorie_3 = 'mairie' then 'mairie'
		when categorie_3 = 'cinéma' then 'salle de spectacles'
		else null
	end as categori_3,
	
	code_commune_insee, 
	code_departement_insee, 
	score_fiabilite, 
	"source", 
	id_source_brut, 
	id_source_corrige, 
	id_source,
	methode_appariement_retenue, 
	idtup, 
	catpro3, 
	denomination, 
	libelle_adresse_principale, 
	siret_principal, 
	propriete_public_probable, 
	usage_public, 
	categorie_public, 
	erp, 
	categorie_erp


FROM 
    groupe_id_tup_id_source	gr
where rn = 1	



);


CREATE INDEX idx_code_departement_insee ON "work".joinining_sources (code_departement_insee);
CREATE INDEX idx_rel_id_source ON "work".joinining_sources (id_source);