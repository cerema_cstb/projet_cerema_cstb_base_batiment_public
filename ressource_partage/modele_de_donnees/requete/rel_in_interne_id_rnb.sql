DROP TABLE IF EXISTS z_tmp_mhamdoune.rel_id_detail_id_rnb;
CREATE TABLE z_tmp_mhamdoune.rel_id_detail_id_rnb as (

With unnest_id as (
    SELECT
        id_detail,
        id_synthese,
        ign, 
        unnest(liste_id_rnb) AS rnb_id,
        code_departement_insee
    FROM  z_tmp_mhamdoune.coriandre_detail

),

unique_rnb_id as (
SELECT distinct on (rnb_id)
    rnb_id,
    code_departement_insee,
    geom_rnb_pos as geometrie_rnb

FROM etablissement_public_dev.rel_rnb_parcelle_unifiee 
group by rnb_id, code_departement_insee, geometrie_rnb

)


SELECT 
    id_detail,
    id_synthese,
    ign, 
    un.rnb_id,
    un.code_departement_insee,
    geometrie_rnb
FROM unnest_id un
    inner JOIN unique_rnb_id rel
    On rel.rnb_id = un.rnb_id
    AND rel.code_departement_insee = un.code_departement_insee

where un.rnb_id is not null

);


CREATE INDEX index_code_departement_insee_rnb_interne ON z_tmp_mhamdoune.rel_id_detail_id_rnb (code_departement_insee);
CREATE INDEX index_id_synthese_rnb_interne  ON z_tmp_mhamdoune.rel_id_detail_id_rnb (id_synthese);
CREATE INDEX index_geom_rnb_interne  ON z_tmp_mhamdoune.rel_id_detail_id_rnb USING GIST (geometrie_rnb);

ALTER TABLE z_tmp_mhamdoune.rel_id_detail_id_rnb
ADD CONSTRAINT pk_id_interne_rnb_interne  PRIMARY KEY (id_detail, rnb_id);