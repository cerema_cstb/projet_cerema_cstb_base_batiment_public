DROP TABLE IF EXISTS z_tmp_mhamdoune.coriandre_synthese;
CREATE TABLE z_tmp_mhamdoune.coriandre_synthese as (

with grouby_by_id_synthese as (

	select 
	
		*, 
		ROW_NUMBER() OVER (
            PARTITION BY id_synthese
            ORDER BY 
            	CASE 
		            WHEN ign = TRUE THEN 0 -- Priorité aux lignes où ign est TRUE
		            ELSE 1
		        END,
                CASE 
                    WHEN source = 'admin express' THEN 1
                    WHEN source = 'basilic' THEN 2
                    WHEN source = 'aen' THEN 3
                    WHEN source = 'pies' THEN 4
                    WHEN source = 'finess' THEN 6
                    WHEN source = 'adm' THEN 5
                    ELSE 7
                END
        ) AS rn
        
	from z_tmp_mhamdoune.coriandre_detail
	
)


select 
	
	id_synthese,
	liste_id_rnb,
    geometrie_rnb,
    ign, 
    categorie_2,
    categorie_3,
    code_commune_insee,
    code_departement_insee,
    score_fiabilite::text,
    "source",
    id_source_brut, 
    id_source_corrige,
    denomination,
    libelle_adresse_principale,
    siret_principal,
    propriete_public_probable,
    usage_public,
    categorie_public,
    erp,
    categorie_erp


from grouby_by_id_synthese
where rn = 1 and liste_id_rnb is not null
order by code_departement_insee ASC

);


CREATE INDEX index_code_departement_insee_table_synthese ON z_tmp_mhamdoune.coriandre_synthese (code_departement_insee);

ALTER TABLE z_tmp_mhamdoune.coriandre_synthese
ADD CONSTRAINT pk_id_interne_table_synthese PRIMARY KEY (id_synthese);
